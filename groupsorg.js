Drupal.clueTipAttach = function() {
  $('a.load-local').cluetip({local:true, sticky:true, closePosition: 'title', cursor: 'pointer'});
  $("html > *").not("a").click(function() {$("#cluetip-close").triggerHandler("click");});
}

if (Drupal.jsEnabled) {
  $(document).ready(Drupal.clueTipAttach);
}
