<?php

include_once('groupsorg.features.inc');

function groupsorg_menu_alter(&$items) {
  // Do not allow anyone to add users to a group without permission.
  $items['og/users/%node/add_user']['access callback'] = 'groupsorg_can_add_users';
  $items['og/users/%node/add_user']['access arguments'] = array(2);
}

/**
 * Grants access to site admins.
 */
function groupsorg_can_add_users($group_node) {

  global $user;

  if (is_array($user->roles) && in_array('admin', $user->roles)) {
    return TRUE;
  }

  return FALSE;

}

function groupsorg_preprocess_node(&$variables) {
  $node = $variables['node'];
  if (og_is_group_type($node->type)) {
    // We handle this in own template.
    unset($variables['terms']);
  }
  // Make "submitted by" data on wiki pages more collaborated.
  if ($variables['node']->type == 'wikipage') {
    $variables['user_picture'] = '';
    $variables['picture'] = '';
    $last_author = theme('username', user_load($variables['node']->revision_uid));
    $last_edition = $variables['node']->revision_timestamp;
    $variables['submitted'] .= '<br/>' . t('Last updated by !username on @date', array('!username' => $last_author, '@date' => format_date($last_edition)));
  }

}

/**
 * Implementation of hook_user().
 */
function groupsorg_user($op, &$edit, &$account, $category = NULL) {
  if ($op == 'view') {
    // Manager of groups.
    if (!empty($account->og_groups)) {
      // Collect a count of groups user is an admin of. @todo this could instead
      // be a list of those groups, but OG doesn't show group names on a user's
      // profile unless they are an admin or are the account being seen.
      $count = 0;
      foreach ($account->og_groups as $group) {
        if ($group['is_admin']) {
          $count++;
        }
      }
      $account->content['summary']['group_manager'] = array(
        '#type' => 'user_profile_item',
        '#title' => t('Organizer of groups'),
        '#value' => $count,
      );
    }
    // Count of submitted events.
    $result = db_fetch_array(db_query(db_rewrite_sql("SELECT COUNT(1) AS count FROM {node} n WHERE n.status = 1 AND n.type = 'event' AND n.uid = %d"), $account->uid));
    if (!empty($result) && (int) $result['count'] > 0) {
      $account->content['summary']['submitted_events'] = array(
        '#type' => 'user_profile_item',
        '#title' => t('Submitted events'),
        '#value' => $result['count'],
      );
    }
    // Count of events user is listed as a co-organizer of.
    $field = content_fields('field_organizers', 'event');
    $db_info = content_database_info($field);
    $sql = "SELECT COUNT(1) as count FROM {node} n INNER JOIN {" . $db_info['table'] . "} f ON n.vid = f.vid WHERE n.status = 1 AND n.type = 'event' AND f.field_organizers_uid = %d";
    $result = db_fetch_array(db_query(db_rewrite_sql($sql), $account->uid));
    if (!empty($result) && (int) $result['count'] > 0) {
      $account->content['summary']['coorganized_events'] = array(
        '#type' => 'user_profile_item',
        '#title' => t('Co-organized events'),
        '#value' => $result['count'],
      );
    }
    // Count of votes by this user.
    $result = db_fetch_array(db_query("SELECT COUNT(1) AS count FROM {votingapi_vote} v WHERE v.uid = %d", $account->uid));
    if (!empty($result) && (int) $result['count'] > 0) {
      $account->content['summary']['voted_count'] = array(
        '#type' => 'user_profile_item',
        '#title' => t('Number of times voted'),
        '#value' => $result['count'],
      );
    }
  }
}

function phptemplate_image_attach_body($node) {
  return $node->body;
}

/**
 * Implements hook_og_links_alter().
 *
 */
function groupsorg_og_links_alter(&$links, $node) {
  //  We now provide this as a block of multiple users.
  unset($links['manager']);

  // @TODO
  // Only Knight Foundation group can make proposals.
  // For now handled at the permission level since Knight foundation is inactive.
  // if ($node->nid != 9328) {
  //   unset($links['create_proposal']);
  // }

}

function groupsorg_menu() {
  // @todo: remove this, now handled in a pure view.
  $items['local-ambassadors'] = array(
    'title' => 'Local ambassadors',
    'page callback' => 'groupsorg_ambassadors',
    'access' => TRUE,
    'type' => MENU_CALLBACK,
  );
  // @todo: remove this someday, could be handled in a pure view.
  $items['group/anon'] = array(
    'title' => t('Recent'),
    'page callback' => 'views_page',
    'page arguments' => array('og_tracker', 'default'), // 2 last arg?
    'access callback' => 'user_is_anonymous',
  );

  return $items;
}

/**
 * moshe wants it this way. most people should see our tower of babel.
 */
function groupsorg_cron() {
  db_query("UPDATE {users} SET language = NULL WHERE language = 'en'");
  // @todo: pull in our mollom fixing query here?
}

/**
 * Implements hook_help().
 */
function groupsorg_help($path, $arg) {
  global $user;
  switch ($path) {
    case 'og':
      return '<ul> <li>You are welcome to <a href="/groups/create">create a group</a>. Please adhere to the guidelines presented there. <li>You may subscribe to the <a href="/og/all/feed">RSS feed</a> for this groups directory </ul>';
    case 'group':
      return t('Shows unread posts in subscribed groups.');
      case 'group/mytracker':
        return t('Shows posts in subscribed groups.');
    case 'group/tracker':
      $output = "This page lists all posts across this whole web site, regardless of your subscriptions.";
      if ($user->uid == 0) {
        $output .= '<p>'. t('Note that you have many more opportunities for subscription and reading if you are a logged in user to this site.  You might wish to <a href="/user/login">login</a> or <a href="/user/register">register</a> now.'). '</p>';
      }
      return $output;
    case 'event':
      $items[] = l(t('Create an event'), 'node/add/event'). t('. You may optionally affiliate your event with groups.');
      $items[] = t('You might be interested in our site-wide <a href="!url">Ical feed</a>. Also, each group maintains its own feed. Look for the <a href="!url"><img src="/sites/all/modules/date/images/ical16x16.gif" alt="calendar icon"></a>&nbsp; in the Group Events block.', array('!url' => url('ical')));
      return (theme('item_list', $items));
    case 'node/add/og':
      // @todo: we should move this to regular configuration somehwere (a block) so admins can edit.
      // It's here now because in here we can get html.
      $output = "We have some guidelines about groups:

<ul>
<li> Group names should be brief and humble and avoid the used of the word <em>Drupal</em>. Some examples
<ul>
<li> <strong>Good</strong>: Installation. San Francisco
<li> <strong>Bad</strong>: Installation group. San Francisco League of Drupal fans
</ul>
<li> Don't overlap much with an existing group. We want fewer big groups given a choice. Groups can split apart organically after some time.
<li> Provide meaningful description and welcome message.
<li> The focus of your group should be drupal related discussion. We don't host groups that discuss other topics like politics and fundraising. Drupal can be popular for powering sites like those, but thats beside the point. We discuss Drupal here, exclusively.
<li> Feel free to use your native language instead of English for all group fields, except for the title and the group description.  Please provide these two fields in English in all cases.
<li> Almost all groups should be <strong>open</strong>. Use other choices only when needed.
<li> All groups are subject to approval by moderators. Please be patient.
<li> Groups should fall into one of three categories:
<ul>
<li> working group coordinating on a Drupal feature, module, or distribution
<li> a geographical user-group such as users in a specific city
<li> or a Drupal event organizing group such as a DrupalCON organizing group
</ul>
</ul>
";
    return $output;
  }
}

/**
 * Implements hook_nodeapi().
 */
function groupsorg_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {

  switch ($op) {
    case 'view':
      // Encourage people to be bold and edit wiki pages.
      if ($node->type == 'wikipage' && $a4 && $_SERVER['REQUEST_METHOD'] == 'GET') {
        if (node_access('update', $node)) {
          drupal_set_message(t('You are viewing a wiki page. You are welcome to <a href="!edit">edit it</a>. <a href="http://en.wikipedia.org/wiki/WP:BB">Be bold!</a>', array('!edit' => url("node/$node->nid/edit"))));
        }
        else {
          if ($group_node = og_get_group_context())  {
            drupal_set_message(t('You are viewing a wiki page. You are welcome to <a href="!join">join the group and then edit it</a>. <a href="http://en.wikipedia.org/wiki/WP:BB">Be bold!</a>', array('!join' => url("og/subscribe/$group_node->nid", array('query' => drupal_get_destination())))));
          }
        }
      }

      if ($node->type == 'og' && $a4) {

        // Check if group needs moderation.

        $flag_query = "SELECT COUNT(0) AS count
        FROM {flag_content} fc
        JOIN {flags} f
          ON fc.fid = f.fid
        WHERE f.name = 'needs_moderation'
        AND fc.content_type = 'node'
        AND fc.content_id = %d";
        $result = db_fetch_array(db_query($flag_query, $node->nid));
        if (
          (!empty($result)) &&
          ((int) $result['count'] > 0)
        ) {
          drupal_set_message(t('This group is pending moderation. See <a href="!documentation">documentation on this process</a>.', array('!documentation' => 'https://www.drupal.org/node/2346927')), 'warning');
        }

        // Check if group needs more organizers.

        $organizer_query = "SELECT COUNT(uid) AS count, is_admin
        FROM og_uid
        WHERE nid = %d
        GROUP BY is_admin";
        $result = db_query($organizer_query, $node->nid);
        $members = array();

        while ($row = db_fetch_array($result)) {
          $members[$row['is_admin']] = $row['count'];
        }

        if (
          ($members[1] < 2) &&
          ($members[0] > 19)
        ) {

          drupal_set_message(t('This group should probably have more organizers. See <a href="!documentation">documentation on this recommendation</a>.', array('!documentation' => 'https://www.drupal.org/node/2346959')), 'warning');

        }
      }

      break;
    case 'insert':
      // Flag new groups as needing moderation.
      if ($node->type == 'og') {
        $flag = flag_get_flag('needs_moderation');
        if ($flag) {
          $flag->flag('flag', $node->nid, NULL, TRUE);
        }
      }
      break;
  }
}

/**
 * Implements hook_db_rewrite_sql().
 */
function groupsorg_db_rewrite_sql($query, $primary_table, $primary_field, $args) {

  global $user;

  switch ($primary_field) {

    case 'nid':

      // This query deals with nodes

      $access = (user_access('administer nodes') || user_access('moderate content'));

      if (!$access && $query) {
        return array(
          'where' => "($primary_table.nid NOT IN (SELECT groupsorg_fc.content_id FROM flag_content groupsorg_fc INNER JOIN flags groupsorg_f ON groupsorg_fc.fid = groupsorg_f.fid WHERE groupsorg_f.name = 'needs_moderation'))",
        );
      }

      break;
  }

}

/**
 * Local ambassadors page (/local-ambassadors).
 */
function groupsorg_ambassadors() {
  // @todo remove all this, it's handled by a view now.
  // Number of columns to display.
  $columns = 5;

  // Number of rows to display.
  $rows = 10;

  // Pager id. For using more than one pager on the same page.
  $pager_id = 0;

  $per_page = $rows * $columns;

  $sql_from = 'FROM {users} u JOIN {content_field_organizers} cfo ON u.uid = cfo.field_organizers_uid JOIN {content_type_event} cte ON cfo.nid = cte.nid AND cfo.vid = cte.vid';

  $sql = 'SELECT DISTINCT u.* '. $sql_from .' ORDER BY cte.field_start7_value DESC';

  $count_sql = 'SELECT COUNT(DISTINCT(u.uid)) AS row_count '. $sql_from;

  $result = pager_query($sql, $per_page, $pager_id, $count_sql);

  $output = t('<p>The following people are local Drupal ambassadors and are currently helping to organize a local Drupal event.  To learn how you can help organize a local Drupal event visit the <a href="http://drupal.org/node/247945">organizing events tips and guidelines handbook</a>.  To see if there is a local group near you view this <a href="http://tinyurl.com/57r8tw">map</a>.</p>');

  if (isset($result)) {
    drupal_add_css(drupal_get_path('module', 'og'). '/og.css');
    $output .= theme('og_picture_grid', $result, $columns);
    $output .= theme('pager', NULL, $per_page, $pager_id);
  }

  return $output;
}

function phptemplate_signup_user_form() {
  global $user;

  // This line is required for this form to function -- DO NOT EDIT OR REMOVE.
  $form['signup_form_data']['#tree'] = TRUE;

  // MW: clutter
  // $form['signup_form_data']['Name'] = array(
  //     '#type' => 'textfield',
  //     '#title' => t('Name'),
  //     '#size' => 40, '#maxlength' => 64,
  //     '#required' => true,
  //   );
  //   $form['signup_form_data']['Phone'] = array(
  //     '#type' => 'textfield',
  //     '#title' => t('Phone'),
  //     '#size' => 40, '#maxlength' => 64,
  //   );

  // If the user is logged in, fill in their name by default.
  if ($user->uid) {
    $form['signup_form_data']['Name']['#default_value'] = $user->name;
  }

  return $form;
}

// See sprites.module
function groupsorg_sprites_images_ignore() {
  // @todo: remove this?
}

/**
 * Implements hook_form_alter().
 */
function groupsorg_form_alter(&$form, $form_state, $form_id) {
  // For group admins on node forms that are a group type where they are editing an existing post, make the group selection box "big".
  if ($form['#id'] == 'node-form' && $form_id == $form['type']['#value'] .'_node_form' && !empty($form['nid']['#value']) && user_access('administer organic groups') && og_is_group_post_type($form['type']['#value'])) {
    if (isset($form['og_nodeapi']['visible'])) {
      // 20. That's big.
      $form['og_nodeapi']['visible']['og_groups']['#size'] = 20;
    }
  }
  // Prevent users from signing up for nodes that require attendees' full name.
  if ($form_id == 'signup_form') {
    global $user;
    $account = user_load($user->uid);
    $node = node_load($form['nid']['#value']);
    if (strpos($node->body, '<require_full_name />') !== FALSE && empty($account->profile_full_name)) {
      // Disable the Signup button.
      $form['collapse']['submit']['#access'] = FALSE;
      $edit_url = 'http://' . $account->init . '/Personal%20information';
      // We wrap this in a div to prevent weird results from Drupal.behaviors.collapse.
      $form['collapse']['require_full_name'] = array(
  	'#type' => 'markup',
  	'#value' => '<div>' . t('You must <a href="@edit-profile">provide your full name</a> to sign up for this event.', array('@edit-profile' => $edit_url)) . '</div>',
      );
    }
  }

  // Hide checkboxes "Only my signups" and "Only events in my group" from anonymous users in the /events list
  if ($form_id == 'views_exposed_form' && $form['#id'] == 'views-exposed-form-event-list-page-1' && user_is_anonymous()) {
    $form['uid']['#access'] = FALSE;
    $form['signup_user_current']['#access'] = FALSE;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function groupsorg_form_aggregator_admin_settings_alter(&$form) {
  // Add a "32 weeks" item to the Aggregator module's list of options
     $form['aggregator_clear']['#options']['19353600'] = t('32 weeks');
}

function groupsorg_preprocess_views_view($vars) {
  $view = $vars['view'];
  if ($view->name === 'event_list') {
    $headline = '';
    $timestamp_now = date_format(date_now(), 'U');
    foreach ($view->result as $key => $result) {
      // Note: The timestamps are in UTC/the site default timezone.

      // Remove events that are finished - the end date has passed.
      if ($timestamp_now > $result->node_data_field_start7_field_start7_value2) {
        unset($view->result[$key]);
        continue;
      }

      // Set the header according to the start date.
      $text = _groupsorg_views_events_date_group($result->node_data_field_start7_field_start7_value);
      if ($headline != $text) {
        $headline = $text;
        $vars['view']->style_plugin->rendered_fields[$key]['nothing'] = '<h2>' . $text . '</h2>';
      }
    }

    if ($headline) {
      $html = $view->style_plugin->render($view->result);
      $vars['rows'] = $html;
    }
  }
}

function _groupsorg_views_events_date_group($timestamp) {
  $now = date_now();
  // Note: The timestamp is in UTC/the site default timezone.
  $my_date = date_make_date($timestamp, NULL, DATE_UNIX);
  $date = date_format_date($my_date, 'custom', 'Y-m-d');

  // Passed/finished events are removed in groupsorg_preprocess_views_view above,
  // but events started yesterday and running over two days might still be in the view.
  if ($date === date_format_date(date_modify(drupal_clone($now), '-1 day'), 'custom', 'Y-m-d')) {
    return t('Yesterday');
  }

  if ($date === date_format_date($now, 'custom', 'Y-m-d')) {
    return t('Today');
  }

  if ($date === date_format_date(date_modify(drupal_clone($now), '+1 day'), 'custom', 'Y-m-d')) {
    return t('Tomorrow');
  }

  $next_monday = date_format_date(date_modify(drupal_clone($now), '+1 Sunday'), 'custom', 'Y-m-d');
  $my_monday = date_format_date(date_modify(drupal_clone($my_date), '+1 Sunday'), 'custom', 'Y-m-d');
  if ($next_monday === $my_monday) {
    return t('Later this week');
  }

  $month = date_format_date($my_date, 'custom', 'F');
  if ($month === date_format_date($now, 'custom', 'F')) {
    return t('Later this month');
  }

  if (date_format_date($my_date, 'custom', 'Y') !== date_format_date($now, 'custom', 'Y')) {
    return t('Next year');
  }

  return t($month);
}
