<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function groupsorg_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "apachesolr" && $api == "apachesolr_environments") {
    return array("version" => 1);
  }
  elseif ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function groupsorg_node_info() {
  $items = array(
    'book' => array(
      'name' => t('Book page'),
      'module' => 'features',
      'description' => t('A <em>book page</em> is a page of content, organized into a collection of related entries collectively known as a <em>book</em>. A <em>book page</em> automatically displays links to adjacent pages, providing a simple navigation system for organizing and reviewing structured content.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'event' => array(
      'name' => t('Event'),
      'module' => 'features',
      'description' => t('An event is a happenning which can be given a start and end date, thus appearing in the events calendar.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => t('<p>Only add actual events with established dates. Proposed events, discussions of event ideas, etc. belong in Discussions. Because content can\'t be moved between types, miscategorized content will be removed.</p>

<p>Only enter the actual date and time of the event. If your event repeats, you must enter each instance separately (for now). Your event post may not specify a range longer than 5 days. This prevents the calendar from being dominated by any one event. If your event lasts longer than 5 days, then please specify a one day duration or 5 day duration and give more detail in the event description.</p>

<p>Commercial events and/or non-Drupal events may only be listed in the Training or Related events categories. These events must have a clear relationship to Drupal. jQuery training is okay, SXSW is okay, Cubs game is not.</p>'),
    ),
    'og' => array(
      'name' => t('Group'),
      'module' => 'features',
      'description' => t('A home page and associated features for working groups and geographical groups to discuss topics related to Drupal.

Groups on this site should be set to have open membership except in very unusual cases.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'page' => array(
      'name' => t('Page'),
      'module' => 'features',
      'description' => t('If you want to add a static page, like a contact page or an about page, use a page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'proposal' => array(
      'name' => t('KDI Proposal'),
      'module' => 'features',
      'description' => t('A proposal for a Knight Drupal Initiative grant.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Please describe your project\'s desired goals and outcomes, including the specific features and improvements that you propose'),
      'min_word_count' => '50',
      'help' => t('<p>Please give a thorough overview of your proposal. What would your project make possible? What would it change in the realm of Drupal? What would that impact be on the world around us?</p?

<p>Also, for your reference:
<ul>
<li><a href="http://groups.drupal.org/node/10462">Tips for writing your proposal</a></li>
<li><a href="http://groups.drupal.org/node/10459">Goals of the Knight Drupal Initiative</a></li>
<li><a href="http://www.knightfoundation.org/about_knight/fact_sheet.dot">About the Knight Foundation</a></li>
<li><a href="http://drupal.org/mission">Drupal\'s Mission</a></li>
</ul>
</p>'),
    ),
    'story' => array(
      'name' => t('Discussion'),
      'module' => 'features',
      'description' => t('A discussion is the typical posting on groups.drupal.org. Use this when you just want to ask a question or make a statement.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'wikipage' => array(
      'name' => t('Wiki page'),
      'module' => 'features',
      'description' => t('Any registered user may edit a wiki page, including non subscribers of the group.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => t('A wiki page may be edited by any registered user, including those who are not subscribed to the group.'),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function groupsorg_views_api() {
  return array(
    'api' => '2',
  );
}
