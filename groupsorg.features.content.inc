<?php

/**
 * Implementation of hook_content_default_fields().
 */
function groupsorg_content_default_fields() {
  $fields = array();

  // Exported field: field_event_type
  $fields['event-field_event_type'] = array(
    'field_name' => 'field_event_type',
    'type_name' => 'event',
    'display_settings' => array(
      'weight' => '-5',
      'parent' => '',
      '4' => array(
        'format' => 'plain',
        'exclude' => 0,
      ),
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => 'usergroup|User group meeting
regional|Drupalcamp or Regional Summit
drupalcon|DrupalCon
virtual|Online meeting (eg. IRC meeting)
training|Training (free or commercial)
sprint|Sprint
related|Related event (ie. not Drupal specific)
releaseparty|Drupal 8 Release Party',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Event type',
      'weight' => '-5',
      'description' => '',
      'type' => 'optionwidgets_select',
      'module' => 'optionwidgets',
    ),
  );

  // Exported field: field_organizers
  $fields['event-field_organizers'] = array(
    'field_name' => 'field_organizers',
    'type_name' => 'event',
    'display_settings' => array(
      'weight' => '-6',
      'parent' => '',
      '4' => array(
        'format' => 'plain',
        'exclude' => 0,
      ),
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'userreference',
    'required' => '0',
    'multiple' => '1',
    'module' => 'userreference',
    'active' => '1',
    'referenceable_roles' => array(
      '4' => 0,
      '5' => 0,
      '6' => 0,
      '2' => 0,
      '3' => 0,
    ),
    'referenceable_status' => '1',
    'advanced_view' => 'users_for_userreference',
    'advanced_view_args' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'uid' => '',
          'error_field' => 'field_organizers][0][user_name',
        ),
      ),
      'default_value_php' => '',
      'autocomplete_match' => 'contains',
      'size' => '60',
      'reverse_link' => 0,
      'label' => 'Organizers',
      'weight' => '-6',
      'description' => 'People who were involved in organizing this event. They will appear on the Drupal ambassadors page.',
      'type' => 'userreference_autocomplete',
      'module' => 'userreference',
    ),
  );

  // Exported field: field_start7
  $fields['event-field_start7'] = array(
    'field_name' => 'field_start7',
    'type_name' => 'event',
    'display_settings' => array(
      'weight' => '-9',
      'parent' => '',
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'field_start7_default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'field_start7_default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'datestamp',
    'required' => '0',
    'multiple' => '0',
    'module' => 'date',
    'active' => '1',
    'granularity' => array(
      'year' => 'year',
      'month' => 'month',
      'day' => 'day',
      'hour' => 'hour',
      'minute' => 'minute',
    ),
    'timezone_db' => 'UTC',
    'tz_handling' => 'date',
    'todate' => 'optional',
    'repeat' => 0,
    'repeat_collapsed' => '',
    'default_format' => 'field_start7_default',
    'widget' => array(
      'default_value' => 'blank',
      'default_value_custom' => '',
      'default_value2' => 'blank',
      'default_value_custom2' => '',
      'input_format' => 'Y-m-d H:i',
      'input_format_custom' => '',
      'increment' => '5',
      'text_parts' => array(),
      'year_range' => '-6:+2',
      'default_value_code' => '',
      'default_value_code2' => '',
      'label_position' => 'above',
      'label' => 'Start',
      'weight' => '-9',
      'description' => '',
      'type' => 'date_popup',
      'module' => 'date',
    ),
  );

  // Exported field: field_url
  $fields['event-field_url'] = array(
    'field_name' => 'field_url',
    'type_name' => 'event',
    'display_settings' => array(
      'weight' => '-4',
      'parent' => '',
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '1',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          'format' => '1',
          '_error_element' => 'default_value_widget][field_url][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Event URL',
      'weight' => '-4',
      'description' => 'If more information about this event is available on another website, add that address here.',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_funding_request
  $fields['proposal-field_funding_request'] = array(
    'field_name' => 'field_funding_request',
    'type_name' => 'proposal',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'hidden',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '1',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => '',
      'rows' => '5',
      'label' => 'What is your total budget estimate and how much funding are you requesting',
      'weight' => '5',
      'description' => 'This information is an informal estimate. A formal budget will be required before your proposal can be funded, but is not required now. If you have other sponsorship or sources of funding, please list them here as well. You may list "private funding" if you do not wish to reveal you funding sources at this time.',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_implementation_plan
  $fields['proposal-field_implementation_plan'] = array(
    'field_name' => 'field_implementation_plan',
    'type_name' => 'proposal',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'hidden',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '1',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => '',
      'rows' => '5',
      'label' => 'How will you implement and distribute your project?',
      'weight' => '4',
      'description' => 'Please list the people and companies involved in the project, their roles and your preliminary plan for releasing and marketing your work.',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_knight_goals
  $fields['proposal-field_knight_goals'] = array(
    'field_name' => 'field_knight_goals',
    'type_name' => 'proposal',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'hidden',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '1',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => '',
      'rows' => '5',
      'label' => 'How does your proposal meet the stated goals of the Knight Drupal Initiative program?',
      'weight' => '1',
      'description' => 'Reference: <a href="http://groups.drupal.org/node/10459">the goals of the Knight Drupal Initiative</a>?',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_timeline
  $fields['proposal-field_timeline'] = array(
    'field_name' => 'field_timeline',
    'type_name' => 'proposal',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'hidden',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '1',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => '',
      'rows' => '5',
      'label' => 'How long will your project take to complete?',
      'weight' => '3',
      'description' => 'Please outline the general schedule for the development and delivery of your project.',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Event URL');
  t('Event type');
  t('How does your proposal meet the stated goals of the Knight Drupal Initiative program?');
  t('How long will your project take to complete?');
  t('How will you implement and distribute your project?');
  t('Organizers');
  t('Start');
  t('What is your total budget estimate and how much funding are you requesting');

  return $fields;
}
